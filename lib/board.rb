class Board
  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  attr_reader :grid

  def initialize(grid=self.class.default_grid)
    @grid = grid
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, mark)
    x, y = pos
    grid[x][y] = mark
  end

  def count
    grid.flatten.count(:s)
  end

  def empty?(pos=nil)
    if pos.nil?
      grid.flatten.none? { |mark| mark == :s }
    else
      self[pos].nil?
    end
  end

  def full?
    grid.flatten.all? { |mark| mark == :s }
  end

  def place_random_ship
    until full?
      rand_pos = [rand(grid.size), rand(grid.first.size)]

      return self[rand_pos] = :s if empty?(rand_pos)
    end

    raise "Board is full!"
  end

  def won?
    grid.flatten.none? { |mark| mark == :s }
  end
end
